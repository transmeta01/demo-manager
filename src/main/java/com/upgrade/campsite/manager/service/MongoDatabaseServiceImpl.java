package com.upgrade.campsite.manager.service;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * Copyright 2021 www.cogalab.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * </p>
 *
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * </p>
 *
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * </p>
 *
 * @author <a href="richard.mutezintare@cogalab.com">Richard M.</a>
 */
public class MongoDatabaseServiceImpl implements MongoDatabaseService {


  @Override
  public JsonObject bookingById(Integer id) {
    return null;
  }

  @Override
  public JsonObject cancelBooking(JsonObject user, JsonObject modificationData) {
    return null;
  }

  @Override
  public JsonObject modifyBooking(JsonObject user, JsonObject toModify) {
    return null;
  }

  @Override
  public JsonObject cancelBooking(Integer id) {
    return null;
  }

  @Override
  public JsonObject modifyBooking(JsonObject toModify) {
    return null;
  }

  @Override
  public JsonObject createBooking(String firstName, String lastName, String email, String start, String end) {
    return null;
  }

  @Override
  public JsonObject createBooking(JsonObject user, String title, String start, String end) {
    return null;
  }

  @Override
  public JsonArray createGroupBooking(JsonObject principal, JsonArray participants, String eventTitle, String start_date, String end_Date) {
    return null;
  }

  @Override
  public JsonObject isCampsiteAvailable(String from, String to) {
    return null;
  }

  @Override
  public JsonObject authenticateUser(String id, String firstName, String lastName, String email) {
    return null;
  }

  @Override
  public JsonObject authenticateUser(JsonObject user) {
    return null;
  }

  @Override
  public JsonObject saveUser(String firstName, String lastName, String email) {
    return null;
  }
}
