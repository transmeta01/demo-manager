package com.upgrade.campsite.manager.service;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.Optional;

/**
 * Copyright 2021 www.cogalab.com
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard.mutezintare@cogalab.com">Richard M.</a>
 */

public interface DatabaseService {

  // @FIXME: old version will be deprecated (put this in openAPI description with detailed info for path to migration.
  JsonObject bookingById(Integer id);

  /**
   * who is do the cancelling? Are they cancelling a booking they own? Is data destruction attack attempt?
   *
   * @param user
   * @param modificationData
   * @return
   */
  JsonObject cancelBooking(JsonObject user, JsonObject modificationData);
  JsonObject modifyBooking(JsonObject user, JsonObject toModify);

  JsonObject cancelBooking(Integer id);

  JsonObject modifyBooking(JsonObject toModify);

  JsonObject createBooking(String firstName, String lastName, String email, String start, String end);

  // @TODO implement me
  JsonObject createBooking(JsonObject user, String title, String start, String end);

  // could it be a good idea to list all the names of the participants (for the organizer) in the camping activity?
  // maybe we could have a feature to email all participants updates via email????????
  // maybe even add a description....could be, if it were a real product!!!!!!!
  // what other useful info(s) "description" could contain!!!!!!!
  // @TODO implement me...
  JsonArray createGroupBooking(JsonObject principal, JsonArray participants, String eventTitle, String start_date, String end_Date);

  /**
   *
   * @param from ISOString debut date
   * @param to   ISOString end  date
   * @return a JsonObject containing all available dates for the month
   */
  JsonObject isCampsiteAvailable(String from, String to);

  JsonObject authenticateUser(String id, String firstName, String lastName, String email);

  JsonObject authenticateUser(JsonObject user);

  JsonObject saveUser(String firstName, String lastName, String email);
}
