package com.upgrade.campsite.manager.service;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Copyright 2021 www.cogalab.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * </p>
 *
 * @author <a href="richard.mutezintare@cogalab.com">Richard M.</a>
 */

public class DatabaseServiceImpl extends AbstractVerticle implements DatabaseService  {

  // fake db, can also use h2 (see pom file for detailed config) in-memory db.
  // Should probably use ConcurrentHashMap<>()...But??????!
//  ConcurrentHashMap<String, JsonObject> betterPerf = new ConcurrentHashMap<>(); ????? Maybe!!!????
  public static Map<String, JsonObject> BookingDB = new HashMap<>();
  public static Map<String, JsonObject> UserDB = new HashMap<>();
  // hazelcast????
  // elastic search????
  // kafka??????
  // h2o????

  enum ModificationCodes {
    CANCEL_BOOKING,
    MODIFY_BOOKING
  }

  // @TODO remove me
//  @Override
//  public JsonObject bookingById(Integer bookingId, JsonObject user) {
//    if (bookingId != null &&  BookingDB.containsKey(bookingId)) {
//      validateU
//      JsonObject booking = BookingDB.get(bookingId);
//    }
//
//    return null;
//  }

  // @FIXME assume the user making the request has been authenticated in the service layer
  @Override
  public JsonObject bookingById(Integer bookingId) {
    if (bookingId != null && BookingDB.containsKey(bookingId)) {
      return BookingDB.get(bookingId);
    }

    return new JsonObject();
  }

  @Override
  public JsonObject cancelBooking(JsonObject user, JsonObject modificationData) {
    // authenticate user
    //
    // verify toModifyBookingExists
    // apply (PATCH op) return the modified data


    return new JsonObject();
  }

  @Override
  public JsonObject modifyBooking(JsonObject user, JsonObject toModify) {
    return new JsonObject();
  }

  @Override
  public JsonObject cancelBooking(Integer id) {
    return new JsonObject();
  }

  @Override
  public JsonObject modifyBooking(JsonObject toModify) {
    return null;
  }

  /**
   * POST operations are guarantied to complete (to some degree), unless they blow up before arriving to this stage.
   * By then it is ok...The DB/data is guarantied to be without corruption. Then, client can always re-tried the operation
   * without adverse consequences, no matter how many times the server denies the request.
   *
   * @param firstName
   * @param lastName
   * @param email
   * @param start
   * @param end
   * @return
   */
  @Override
  public JsonObject createBooking(String firstName, String lastName, String email, String start, String end) {

    JsonArray bookings = new JsonArray();

    JsonObject auth = authenticateUser(new JsonObject().put("first_name", firstName)
                                                      .put("lastname", lastName)
                                                      .put("email", email));
    // validate
    // something more robust may be needed here
    JsonObject user = authenticateUser(auth);
    String bookingId = UUID.randomUUID().toString();

    JsonObject booking = new JsonObject()
      .put("booking_id", bookingId)
      .put("user_id", auth.getString("id"))
      .put("firstName", firstName)
      .put("last_name", lastName)
      .put("email", email)
      .put("start", start)
      .put("end", end);

    // save booking: associate booking with user in Many-to-One relationship
    BookingDB.put(user.getString("user_id"), booking);

    return booking;
  }

  @Override
  public JsonObject createBooking(JsonObject user, String title, String start, String end) {
    if (authenticateUser(user).getString("id") == null) {
      return saveUser(user);
    }
    return createBooking(user, title, start, end);
  }

  // @TODO implement me #ref to Jira ticket
  @Override
  public JsonArray createGroupBooking(JsonObject principal, JsonArray participants, String eventTitle, String start_date, String end_Date) {
    JsonArray queryResult = new JsonArray();

    // validate principal

    // save booking

    // populate return array with all the event's details


    return new JsonArray();
  }

  /**
   * could also have used a named query or preparedStatement style query
   * or JPA connection.query("select from user where firstname = ? and lastname = ? and email = ?") != null; Springboot-Jpa style
   *
   * @param id
   * @param firstName
   * @param lastName
   * @param email
   * @return
   */
  @Override
  public JsonObject authenticateUser(String id, String firstName, String lastName, String email) {
    return UserDB.containsKey(id) ? UserDB.get(id) : new JsonObject();
  }

  @Override
  public JsonObject authenticateUser(JsonObject user) {
    return authenticateUser(new JsonObject().put(user.getString("id"), user));
  }

  @Override
  public JsonObject isCampsiteAvailable(String from, String to) {
    return new JsonObject();
  }

  /**
   * presumably these have already been validated either at the service layer or
   * the handler configuration layer
   *
   * @param firstName
   * @param lastName
   * @param email
   * @return
   */
  @Override
  public JsonObject saveUser(String firstName, String lastName, String email) {
    // @TODO split the users and their id
    JsonObject newUser = new JsonObject()
      .put("first_name", firstName)
      .put("last_name", lastName)
      .put("email", email);

    return saveUser(newUser);
  }

  public JsonObject saveUser(JsonObject user) {
    if(checkForDuplicate(user)) {
      String id = UUID.randomUUID().toString();
      user.put("id", id);

      UserDB.put(id, user);

      return user;
    }

    return new JsonObject();
  }

  /**
   * check for existence of duplicate id/key
   * @param user
   * @return JsonObject
   */
  private boolean checkForDuplicate(JsonObject user) {
    return  (user.getString("id") != null && !UserDB.containsKey(user.getString("id")));
  }

}
