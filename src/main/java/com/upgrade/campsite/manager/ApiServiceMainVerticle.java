package com.upgrade.campsite.manager;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.logging.Logger;

import com.upgrade.campsite.manager.service.DatabaseService;
import com.upgrade.campsite.manager.service.DatabaseServiceImpl;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Promise;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.openapi.RouterBuilder;
import io.vertx.ext.web.validation.RequestParameter;

/**
 * Copyright 2021 www.cogalab.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * </p>
 *
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * </p>
 *
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * </p>
 *
 * @author <a href="richard.mutezintare@cogalab.com">Richard M.</a>
 */

public class ApiServiceMainVerticle extends AbstractVerticle {

  // start and end on the specified day at noon (check-in and check-out time, by default)
  DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy 12:00");

  enum ApiServiceErrorMessages {
    NOT_FOUND("404 Not found"),
    ACCESS_FORBIDDEN("403 access forbidden"),
    GENERIC_ERROR("500 An unexpected error has occurred"),

    OK_200("200 Resource found"),

    A_VALID_EMAIL_IS_REQUIRED_TO_BOOK_THE_CAMPSITE(
      "A valid email is required in order to both the campsite"),
    A_FIRST_NAME_IS_REQUIRED_TO_BOOK_THE_CAMPSITE("your first name is required to book the campsite"),
    A_VALID_LAST_NAME_IS_REQUIRED_TO_BOOK_THE_CAMPSITE("our last name is required to book the campsite"),

    YOU_CAN_ONLY_CANCEL_YOUR_BOOKINGS("You can only cancel your bookings"),
    YOU_CAN_ONLY_MODIFY_YOUR_BOOKINGS("You can only modify your bookings"),
    DEFAULT_MESSAGE("No such message. Please review the key you used");

    // caching
    private static final Map<String, ApiServiceErrorMessages> messageCache = new HashMap<>();
    static {
      for(ApiServiceErrorMessages message : values()) {
        messageCache.put(message.label, message);
      }
    }

    /* return default message if the specified key for a message is incorrect */
    public static ApiServiceErrorMessages getMessage(String key) {
      return messageCache.getOrDefault(key, DEFAULT_MESSAGE);
    }

    public final String label;
    ApiServiceErrorMessages(String label) {
      this.label = label;
    }
  }

  private static final Logger LOGGER = Logger.getLogger("ApiServiceMainVerticle");

  HttpServer httpServer;

  /**
   * @FIXE ME
   * could build an annotation to specify which version of the DB, I want to use
   * or more simply a switch case statement...whatever is most simple and flexible
   **/
  DatabaseService databaseService = new DatabaseServiceImpl();
  // or
//  DatabaseService mongoDatabaseService = new MongoDatabaseServiceImpl();

  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    RouterBuilder.create(this.vertx, "campsite.yaml")
      .onSuccess( routerBuilder -> {

        // map routes to operationId and their respective handlers
        routerBuilder.operation("bookingById").handler(routingContext -> {
          bookingByIdHandler(routingContext);
        });

        routerBuilder.operation("cancelBooking").handler(routingContext -> {
          RequestParameter param =  routingContext.get("bookingId");
          Integer bookingId = param.getInteger();

          // does the booking exists?
//          if(databaseService.validateBooking(bookingId)) {
//            // should probably authenticate the user so people do not randomly cancel other
//            // people's bookings (either out of malice or stupidity...or both)
//          }
        });

        routerBuilder.operation("modifyBooking").handler(routingContext -> {
          JsonObject request = routingContext.getBodyAsJson();
//          if(databaseService.bookingById(request.getInteger("bookingId"))) {
////            if() @TODO implement me
//          } else {
//
//          }
        });

        routerBuilder.operation("createBooking").handler(routingContext -> {
          createBookingHandler(routingContext);
        });

        routerBuilder.operation("availability").handler(routingContext -> {
          // @TODO implement me!!!!!!!
        });

        // generate the router
        Router router = routerBuilder.createRouter();
        // config router with version number
        router.mountSubRouter("/v1", router);

        router.errorHandler(404, routingContext -> {
          JsonObject error = new JsonObject().put(ApiServiceErrorMessages.NOT_FOUND.name(), 404)
            .put("message", (routingContext.failure() != null) ? routingContext.failure().getMessage() :
              ApiServiceErrorMessages.getMessage(ApiServiceErrorMessages.NOT_FOUND.label));
        });

        // initialize and config the server
        httpServer = vertx.createHttpServer(new HttpServerOptions().setPort(8080).setHost("localhost"));
        httpServer.requestHandler(router).listen();

        // complete the verticle start
        startPromise.complete();
      })
    .onFailure(cause -> {
      startPromise.fail(cause);
    });
  }

  private void bookingByIdHandler(RoutingContext context) {
    JsonObject requestObject = context.getBodyAsJson();

    if((requestObject == null) || (!validateUser(requestObject))) {
      context.response()
        .setStatusCode(404)
        .setStatusMessage(ApiServiceErrorMessages.NOT_FOUND.label)
        .end();
    } else {
      JsonObject result = validateBooking(requestObject);
      result.put(ApiServiceErrorMessages.OK_200.label, ApiServiceErrorMessages.OK_200);
      context.response().setStatusCode(200)
        .putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
        .putHeader(HttpHeaders.CONTENT_LOCATION, context.currentRoute().getPath() + "/" + result.getInteger("id"))
        .send(result.encode());
    }

  }

  // POST
  private void createBookingHandler(RoutingContext context) {
    JsonObject requestParam = context.getBodyAsJson();
    String ISOStartDate = requestParam.getString("start_date");
    String ISOEndDate = requestParam.getString("end_date");

    LocalDateTime from = LocalDateTime.parse(ISOStartDate, dateTimeFormatter);
    LocalDateTime to = LocalDateTime.parse(ISOEndDate, dateTimeFormatter);

    String fromString = from.toString();

    // @TODO check if the booking period is more than one month

    if(validateUser(requestParam)) {
      JsonObject result = databaseService.createBooking(requestParam.getString("firstName"),
        requestParam.getString("lastName"),
        requestParam.getString("email"),
        requestParam.getString("from"),
        requestParam.getString("to"));

      context.response().setStatusCode(Integer.parseInt(ApiServiceErrorMessages.OK_200.label))
        .putHeader(HttpHeaders.CONTENT_LOCATION, context.currentRoute().getPath() + "/" + result.getString("id"))
        .send(result.encode());
    }

    // no more than a month (which could be 30 or 31 days) stay


  }

  // POST
  private void cancelBookingHandler(RoutingContext context) {
    // @TODO implement me CANCEL

  }

  private void availabilityHandler(RoutingContext context) {
    // @TODO implement me AVAILABILITY
  }

  // POST
  private void modifyBookingHandler(RoutingContext context) {
    // @TODO implement me MODIFY
  }



  private boolean validateUser(JsonObject request) {
    String firstName = request.getString("FirstName".toLowerCase().trim());
    String lastName = request.getString("lastName".toLowerCase().trim());
    String email = request.getString("email".toLowerCase().trim());
    String key = request.getString("id".toLowerCase(Locale.ROOT).trim());

    // db lookup
     return (firstName != null && firstName.length() > 0) &&
            (lastName!= null && !lastName.isEmpty()) &&
            (email != null && email.isEmpty()) &&
            databaseService.authenticateUser(key, firstName, lastName, email) != null;
  }

  private JsonObject validateBooking(JsonObject request) {
    Integer id = Integer.parseInt(request.getString("bookingById"));
    return databaseService.bookingById(id);

  }


  // is this part of the problem statement ?
  // @TODO verify with Pierre Olivier
  private void scheduleExpirationOfBookings() {

  }

  public static void main(String[] args) {
    Vertx vertx = Vertx.vertx();
    vertx.deployVerticle(new ApiServiceMainVerticle());
  }
}
