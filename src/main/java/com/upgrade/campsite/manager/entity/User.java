package com.upgrade.campsite.manager.entity;

import lombok.Data;

// @Deprecated
@Data
public class User {
  public String firstName;
  public String lastName;
  public String email;

//  public User() {}

  public User(final String firstName, final String lastName, final String email) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
  }
}
